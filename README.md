# RPG Classes
## Important
The project is in Master branch.
## Purpose
This project was created for Noroff .NET Fullstack Accelerate
## Description
This project was made to practice C# basics. It consists of classes to create mock rpg characters and equipment as well as unit tests. The application has a small text based user interface for creating characters.
## Author
Mikko Kaipainen
